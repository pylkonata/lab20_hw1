// requires...
const fs = require('fs');
const path = require('path');
// constants...
const filesPath = "./files";
const fileExtArr = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
function createFile (req, res, next) {  
  if (!req.body.filename) {
    res.status(400).send({ "message": "Please specify 'filename' parameter" });
  } else if (!req.body.content) {
    res.status(400).send({ "message": "Please specify 'content' parameter" });
  } else if (!fileExtArr.includes(path.extname(req.body.filename))) {
    res.status(400).send({
      "message": "Application support only: log, txt, json, yaml, xml, js file extensions"
    });
  } else {
    fs.writeFile(
      path.join(__dirname, 'files', req.body.filename),
      req.body.content,
      (err) => {
        if (err) throw err;
        console.log('Файл был создан');
      }
    );
    res.status(200).send({ "message": "File created successfully" });
  }  
}

function getFiles (req, res, next) {
  let filesArr = [];
  if (!fs.existsSync('files')) {
    res.status(400).send({"message": "Client error"});
  } else {
    fs.readdir(filesPath, (err, files) => {
      try {
        filesArr = files;
        console.log(filesArr);
        res.status(200).send({ "message": "Success", "files": filesArr });
      } catch (err) {
        throw err;
      }
    });
  }
}

const getFile = (req, res, next) => {
  const fileName = req.params.filename;
  const filePath = path.join(__dirname, 'files', fileName);
  
  if (!fs.existsSync(filePath)) {
    res.status(400).send({
      "message": `No file with ${fileName} filename found`
    })
  } else {
    const fileContent = fs.readFileSync(filePath, 'utf8', data => {
      return data;
    });
    const fileExt = path.extname(fileName).split('.')[1];
    const fileStats = fs.statSync(filePath, (error, stats) => {
      if (error) throw error;
      return stats;
    });
    res.status(200).send({
        "message": "Success",
        "filename": fileName,
        "content": fileContent,
        "extension": fileExt,
        "uploadedDate": fileStats.birthtime
      });
  }   
}

// Other functions - editFile, deleteFile

module.exports = {
  createFile,
  getFiles,
  getFile
}
